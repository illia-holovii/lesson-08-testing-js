import CartParser from './CartParser';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	it('should return object with cart items and total price', () => {
		expect(parse(`C:\Users\illia\Documents\Binary Studio Academy\ACADEMY\lesson 08 - Testing JS\samples\cart.csv`)).toEqual({
			"items": [
				{
					"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
					"name": "Mollis consequat",
					"price": 9,
					"quantity": 2
				},
				{
					"id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
					"name": "Tvoluptatem",
					"price": 10.32,
					"quantity": 1
				},
				{
					"id": "33c14844-8cae-4acd-91ed-6209a6c0bc31",
					"name": "Scelerisque lacinia",
					"price": 18.9,
					"quantity": 1
				},
				{
					"id": "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
					"name": "Consectetur adipiscing",
					"price": 28.72,
					"quantity": 10
				},
				{
					"id": "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
					"name": "Condimentum aliquet",
					"price": 13.9,
					"quantity": 1
				}
			],
			"total": 348.32
		});
	});

	// it('should return object with keys from column keys and values from CSV', () => {
	// 	expect(parseLine(`Condimentum aliquet,13.90,1`)).toEqual({
	// 		"id": `${/^[a-zA-Z0-9-]*$/gm}`,
    //         "name": "Condimentum aliquet",
    //         "price": 13.9,
    //         "quantity": 1
	// 	});
	// });
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
});